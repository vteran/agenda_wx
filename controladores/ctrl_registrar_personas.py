#!/usr/bin/env python
#-*-coding:utf-8-*-


######################################################
"""
Agenda WX
---------
Material de apoyo para cursos python dictado por Victor Francisco Terán Herrera.
Contacto: vteran93 at yahoo dot es
Bogota, Colombia

Descripción de la aplicación:
-----------------------------
Es una aplicación que mediante dos tablas de MySQL usando la libreria elixir
almacena los datos asociados a una persona o empresa. Permite almacenar datos 
básicos.

Copyright (C) <2014>  <Víctor Francisco Terán Herrera>


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
######################################################



from modelos.PersonasModel import *
from modelos.TelefonosModel import *

class ctrlPersona():
    def __init__(self):
        pass
    
    
    def insertar_persona(self, tipo_id, identificacion, nombre, fecha_nac, telefonos):
        print tipo_id, identificacion, nombre, fecha_nac
        
        p=Persona(tipo_id=tipo_id, identificacion=identificacion, nombre=nombre, fecha_nac=fecha_nac)
        
        
        telefonos_db=[]
        for telefono in telefonos:
            cod_area=telefono.split("-")[0]
            num_telefono=telefono.split("-")[1]
            t=Telefonos(cod_area=cod_area, telefono=num_telefono)
            telefonos_db.append(t)
            t=None
        p.telefonos=telefonos_db
        session.commit()
        
        return p.id
        
        
    def detalles_persona(self, id_):
        return Persona.getPersonaById(id_)
        
    def listar_personas(self):
        return Persona.getPersonas()
        
    def eliminar_personas(self):
        pass
