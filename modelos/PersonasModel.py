#!/usr/bin/env python
#-*-coding:utf-8-*-


######################################################
"""
Agenda WX
---------
Material de apoyo para cursos python dictado por Victor Francisco Terán Herrera.
Contacto: vteran93 at yahoo dot es
Bogota, Colombia

Descripción de la aplicación:
-----------------------------
Es una aplicación que mediante dos tablas de MySQL usando la libreria elixir
almacena los datos asociados a una persona o empresa. Permite almacenar datos 
básicos.

Copyright (C) <2014>  <Víctor Francisco Terán Herrera>


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
######################################################



from elixir import * 

class Persona(Entity):
    using_options(tablename='personas')
    tipo_id=Field(String(30))
    nombre=Field(String(30))
    identificacion=Field(String(30))
    fecha_nac=Field(DateTime)
    
    telefonos=OneToMany('Telefonos')
    
    def __repr__(self):
        return '<Persona "%s" (%s)>' % (self.nombre, self.identificacion)

    @staticmethod
    def getPersonas():
        lista_personas=Persona.query.all()
        i=0;
        persona_dic={}
        for persona in lista_personas:
            persona_dic[i]=(persona.id, persona.tipo_id, persona.identificacion, persona.nombre, persona.fecha_nac)
            i+=1
        return persona_dic

    @staticmethod
    def getPersonaById(id_):
        #hacer un join
        persona=Persona.query.filter_by(id=id_).all()
        #print (persona)
        informe_persona=""
        informe_persona+="Tipo Identificación: %s \n"%persona[0].tipo_id
        informe_persona+="Identificación: %s \n"%persona[0].identificacion
        informe_persona+="fecha Nacimiento: %s \n"%persona[0].fecha_nac
        informe_persona+="fecha Nacimiento: %s \n"%persona[0].nombre
        telefonos_text=""
        for tel in persona[0].telefonos:
            telefonos_text+= "%s-%s \n" %(tel.cod_area , tel.telefono)
        informe_persona+="Telefonos:\n %s"%telefonos_text
        
        
        return informe_persona
