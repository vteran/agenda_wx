#!/usr/bin/env python
#-*-coding:utf-8-*-
######################################################
"""
Agenda WX
---------
Material de apoyo para cursos python dictado por Victor Francisco Terán Herrera.
Contacto: vteran93 at yahoo dot es
Bogota, Colombia

Descripción de la aplicación:
-----------------------------
Es una aplicación que mediante dos tablas de MySQL usando la libreria elixir
almacena los datos asociados a una persona o empresa. Permite almacenar datos 
básicos.

Copyright (C) <2014>  <Víctor Francisco Terán Herrera>


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
######################################################



from elixir import *
from config import sql_connect
from PersonasModel import Persona
from TelefonosModel import Telefonos
metadata.bind=sql_connect
metadata.bind.echo=True



if __name__=='__main__':
    
    setup_all(True)
    
else:
    setup_all()
    print "INFO|Me conecte a la base de datos"
