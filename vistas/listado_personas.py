#!/usr/bin/env python
#-*-coding:utf-8-*-

######################################################
"""
Agenda WX
---------
Material de apoyo para cursos python dictado por Victor Francisco Terán Herrera.
Contacto: vteran93 at yahoo dot es
Bogota, Colombia

Descripción de la aplicación:
-----------------------------
Es una aplicación que mediante dos tablas de MySQL usando la libreria elixir
almacena los datos asociados a una persona o empresa. Permite almacenar datos 
básicos.

Copyright (C) <2014>  <Víctor Francisco Terán Herrera>


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
######################################################


import wx
import wx.lib.mixins.listctrl as listmix
from wxPython.wx import *
from wx.lib.pubsub import Publisher,pub




from controladores.ctrl_registrar_personas import ctrlPersona
from vistas.detalles_personas import DetallesPersonas
from vistas.registro_personas import RegistroPersonasFrame

menu_titles = [ "Detalles",
                "Modificar",
                "Delete" ]

menu_title_by_id = {}
for title in menu_titles:
    menu_title_by_id[ wxNewId() ] = title


########################################################################
class TestListCtrl(wx.ListCtrl):
 
    #----------------------------------------------------------------------
    def __init__(self, parent, ID=wx.ID_ANY, pos=wx.DefaultPosition,
                 size=(600,300), style=0):#wx.DefaultSize
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
 
########################################################################
class TestListCtrlPanel(wx.Panel, listmix.ColumnSorterMixin):
 
    #----------------------------------------------------------------------
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1, style=wx.WANTS_CHARS)
        
        
        
        
        
        self.list_personas_ctrl = TestListCtrl(self, size=(600,300),
                             style=wx.LC_REPORT
                             |wx.BORDER_SUNKEN
                             |wx.LC_SORT_ASCENDING
                             )
                             
        self.list_personas_ctrl.InsertColumn(0, "id persona")
        self.list_personas_ctrl.InsertColumn(1, "Tipo identificacion", wx.LIST_FORMAT_RIGHT)
        self.list_personas_ctrl.InsertColumn(2, "Identificacion")
        self.list_personas_ctrl.InsertColumn(3, "Nombres")
        self.list_personas_ctrl.InsertColumn(4, "Fecha nacimiento")       
        
        
        
        
        listmix.ColumnSorterMixin.__init__(self, 15)
        self.Bind(wx.EVT_LIST_COL_CLICK, self.OnColClick, self.list_personas_ctrl)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.list_personas_ctrl, 0, wx.ALL|wx.EXPAND, 5)
        self.SetSizer(sizer)
 
    #----------------------------------------------------------------------
    # Used by the ColumnSorterMixin, see wx/lib/mixins/listctrl.py
    def GetListCtrl(self):
        return self.list_personas_ctrl
 
    #----------------------------------------------------------------------
    def OnColClick(self, event):
        print "column clicked"
        event.Skip()

    
        
    

    
 
########################################################################
class ListadoPersonas(wx.Frame):
 
    #----------------------------------------------------------------------
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, "Listado de Personas")
        self.personas_db=[]
        # Add a panel so it looks the correct on all platforms
        self.panel = TestListCtrlPanel(self)
        
        menubar = wx.MenuBar()
        fileMenu = wx.Menu()
        fitem_nuevo = fileMenu.Append(wx.ID_NEW, 'Nuevo', 'Crea una nueva persona')
        fitem_salir = fileMenu.Append(wx.ID_EXIT, 'Salir', 'Salir de la aplicación')
        menubar.Append(fileMenu, '&Archivo')   
        self.SetMenuBar(menubar)
        self.Bind(wx.EVT_MENU, self.CrearNuevaPersona, fitem_nuevo)
        self.panel.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.OnRightClick, self.panel.list_personas_ctrl)
        self.CreateStatusBar()
        self.SetInitialSize()
        self.OnLoad()


    def MakeUI(self):
        #TODO separar la creación del GUI de la aplicación del __init__
        pass
        
    def CrearNuevaPersona(self, event):
        print "Creando nueva persona"
        frame=RegistroPersonasFrame(None, title="Registro de Personas")
        frame.Show()
        
        

    def MenuSelectionCb( self, event ):
        # do something
        operation = menu_title_by_id[ event.GetId() ]
        target    = self.list_item_clicked
        print 'Perform "%(operation)s" on "%(target)s"' % vars()
        if operation=="Detalles":
            #import pdb; pdb.set_trace()

            id_=self.panel.list_personas_ctrl.GetItemText(2)
            print "id Listo a enviar:%s"%(target)
            
            frame_detalles=DetallesPersonas()
            frame_detalles.Show()
            #Siempre que un dato se vaya a enviar a otro frame con sendMessage
            #se debe de enviar despues de hacer el Show, de lo contario
            #el mensaje no va a llegar
            Publisher().sendMessage(("show.detalles"), target)
    
    def OnRightClick(self, event):
        # record what was clicked
        #import pdb; pdb.set_trace()
        if len(self.personas_db)>0:
            self.list_item_clicked = right_click_context = event.GetText()

        ### 2. Launcher creates wxMenu. ###
            menu = wxMenu()
            for (id,title) in menu_title_by_id.items():
            ### 3. Launcher packs menu with Append. ###
                menu.Append( id, title )
            ### 4. Launcher registers menu handlers with EVT_MENU, on the menu. ###
                EVT_MENU( menu, id, self.MenuSelectionCb )

        ### 5. Launcher displays menu with call to PopupMenu, invoked on the source component, passing event's GetPoint. ###
            self.PopupMenu( menu, event.GetPoint() )
            menu.Destroy() # destroy to avoid mem leak
        else:
            pass
    
    def _GetPersonas(self):
        p=ctrlPersona()
        personas_db=p.listar_personas()
        return personas_db   
        
    def OnLoad(self):
        self.personas_db=self._GetPersonas()
        items = self.personas_db.items()
        index = 0
        
        if(len(self.personas_db)>0):
            for key, data in items:
                self.panel.list_personas_ctrl.InsertStringItem(index, unicode(data[0]))
                self.panel.list_personas_ctrl.SetStringItem(index, 1, unicode(data[1]))
                self.panel.list_personas_ctrl.SetStringItem(index, 2, unicode(data[2]))
                self.panel.list_personas_ctrl.SetStringItem(index, 3, unicode(data[3]))
                self.panel.list_personas_ctrl.SetStringItem(index, 4, unicode(data[4]))
                self.panel.list_personas_ctrl.SetItemData(index, key)
                index += 1
        
            # Now that the list exists we can init the other base class,
            # see wx/lib/mixins/listctrl.py
            self.itemDataMap = self.personas_db
            
        else:
            self.PushStatusText("No hay personas registradas")
            return    
        
        
        
#----------------------------------------------------------------------
# Run the program
if __name__ == "__main__":
    app = wx.App(False)
    frame = ListadoPersonas()
    frame.Show()
    app.MainLoop()
