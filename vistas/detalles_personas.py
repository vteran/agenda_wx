#!/usr/bin/env python
#-*-coding:utf-8-*-


######################################################
"""
Agenda WX
---------
Material de apoyo para cursos python dictado por Victor Francisco Terán Herrera.
Contacto: vteran93 at yahoo dot es
Bogota, Colombia

Descripción de la aplicación:
-----------------------------
Es una aplicación que mediante dos tablas de MySQL usando la libreria elixir
almacena los datos asociados a una persona o empresa. Permite almacenar datos 
básicos.

Copyright (C) <2014>  <Víctor Francisco Terán Herrera>


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
######################################################





from controladores.ctrl_registrar_personas import ctrlPersona
from wx.lib.pubsub import Publisher, pub
import sys
import wx




class MyPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent)

class DetallesPersonas(wx.Frame):
 
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, "wxPython Redirect Tutorial")
 
        # Add a panel so it looks the correct on all platforms
        self.panel = MyPanel(self)
        self.log = wx.TextCtrl(self.panel, wx.ID_ANY, size=(300,100),
                          style = wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        btn_editar = wx.Button(self.panel, -1, 'Editar!')
        self.Bind(wx.EVT_BUTTON, self.onButton, btn_editar)
        #import pdb; pdb.set_trace();
        Publisher().subscribe(self.OnCharge, ("show.detalles"))
        #wx.FutureCall(500,self.OnLoad) #1/2 seconds from now to call OnLoad() 
        
        
        # Add widgets to a sizer        
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.log, 1, wx.ALL|wx.EXPAND, 5)
        sizer.Add(btn_editar, 0, wx.ALL|wx.CENTER, 5)
        self.panel.SetSizer(sizer)

        # redirect text here
        #redir=RedirectText(log)
        #sys.stdout=redir

    def onButton(self, event):
        """"Editar"""
        #print "You pressed the button!"
    
    
    def OnCharge(self, msg):
        print "mensaje recibido %s" % msg
        p=ctrlPersona()
        informe=p.detalles_persona(int(msg.data))
        self.log.SetValue(informe)
        
        
        #self.PushStatusText("Telefonos %s almacenados en memoria " %(",".join(self.telefonos)))
    
 
# Run the program
if __name__ == "__main__":
    app = wx.PySimpleApp()
    frame = DetallesPersonas().Show()
    app.MainLoop()
