#!/usr/bin/env python
#-*-coding:utf-8-*-

######################################################
"""
Agenda WX
---------
Material de apoyo para cursos python dictado por Victor Francisco Terán Herrera.
Contacto: vteran93 at yahoo dot es
Bogota, Colombia

Descripción de la aplicación:
-----------------------------
Es una aplicación que mediante dos tablas de MySQL usando la libreria elixir
almacena los datos asociados a una persona o empresa. Permite almacenar datos 
básicos.

Copyright (C) <2014>  <Víctor Francisco Terán Herrera>


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
######################################################



import re

import wx
from wx.lib.pubsub import Publisher

from controladores.ctrl_registrar_personas import ctrlPersona
from agregar_telefono import AgregarTelefonos

class RegistroPersonas(wx.App):
    def OnInit(self):
        self.frame=RegistroPersonasFrame(None, title='Registro De personas')
        #self.SetTop.Window(self.frame)
        self.frame.Show()


        return True

class RegistroPersonasFrame(wx.Frame):
    def __init__(self, parent, *args, **kwargs):
        super(RegistroPersonasFrame, self).__init__(parent,*args, **kwargs)
        # Attributes
        self.panel = wx.Panel(self)
        self.telefonos=None
        Publisher().subscribe(self.showFrame, ("show.mainframe"))
        # Layout
        self._tipo_id_lbl = wx.StaticText(self, label="Tipo Identificación")
        self._tipo_id= wx.ComboBox(self,
                               -1,
                               size=(150,30),
                                choices=["Cedula", "Nit"],
                                style=wx.CB_DROPDOWN)
       
        self._tipo_id_lbl.SetBackgroundColour(wx.Colour(127, 255, 0))
        vsizer = wx.BoxSizer(wx.VERTICAL)
        
        tipo_id_sz = wx.BoxSizer(wx.HORIZONTAL)
        #tipo_id_sz.AddSpacer(10)
        tipo_id_sz.Add(self._tipo_id_lbl)
        #tipo_id_sz.AddSpacer(10)
        tipo_id_sz.Add(self._tipo_id)
        #tipo_id_sz.AddSpacer(10)
        vsizer.Add(tipo_id_sz, -1, wx.EXPAND)
        
        self._identificacion_lbl = wx.StaticText(self, label="Identificación")
        self._identificacion = wx.TextCtrl(self, size=(150,30))
        identificacion_sz = wx.BoxSizer(wx.HORIZONTAL)
        #identificacion_sz.AddSpacer(10) #espacio desde la ventana
        identificacion_sz.Add(self._identificacion_lbl)
        #identificacion_sz.AddSpacer(10) #espacio entre label y caja
        identificacion_sz.Add(self._identificacion)
        vsizer.Add(identificacion_sz, -1, wx.EXPAND)
        
        #vsizer.AddSpacer(10)# separacion vertical
        self._nombres_lbl = wx.StaticText(self, label="Nombres")
        self._nombres = wx.TextCtrl(self ,size=(150,30))
        nombres_sz = wx.BoxSizer(wx.HORIZONTAL)
        #nombres_sz.AddSpacer(10)
        nombres_sz.Add(self._nombres_lbl)
        #snombres_sz.AddSpacer(10)
        nombres_sz.Add(self._nombres)
        #nombres_sz.AddSpacer(10)
        #vsizer.AddSpacer(10)
        vsizer.Add(nombres_sz,-1,wx.EXPAND)
        
        #vsizer.AddSpacer(10)# separacion vertical
        self._fecha_lbl = wx.StaticText(self, label="Fecha de Cumpleaños")
        self._fecha = wx.TextCtrl(self, size=(150,30))
        fecha_sz = wx.BoxSizer(wx.HORIZONTAL)
        #fecha_sz.AddSpacer(10)
        fecha_sz.Add(self._fecha_lbl)
        #fecha_sz.AddSpacer(10)
        fecha_sz.Add(self._fecha)
        #fecha_sz.AddSpacer(10)
        
        
        #fecha_sz.Add(self.btn1)
        #vsizer.AddSpacer(10)
        vsizer.Add(fecha_sz,-1,wx.EXPAND)

        self.btn1 = wx.Button(self, -1,"Guardar")
        self.Bind(wx.EVT_BUTTON, self.Insertar, self.btn1)
        botones_sz=wx.BoxSizer(wx.HORIZONTAL)
        botones_sz.Add(self.btn1,-1, wx.EXPAND)
        
        self.telefonos = wx.Button(self, -1,"Telefonos")
        self.Bind(wx.EVT_BUTTON, self.agregar_telefono, self.telefonos)
        #botones_sz=wx.BoxSizer(wx.HORIZONTAL)
        botones_sz.Add(self.telefonos,-1, wx.EXPAND)
        
        
        vsizer.AddSpacer(10)# separacion vertical
        vsizer.Add(botones_sz, -1, wx.EXPAND )
        
        vsizer.AddSpacer(10)
        
        principal_sizer=wx.BoxSizer(wx.VERTICAL)
        principal_sizer.Add(vsizer)
        self.SetSizer(principal_sizer)
        self.SetSize((500, 400))
        self.CreateStatusBar()
        self.SetInitialSize()
    
    def Insertar(self, evt):
        if not self.ValidaId(self._identificacion.GetValue()):
            self.PushStatusText("La forma de identificacion no es valida")
            return
        
        if len(self.telefonos)==0:
           self.PushStatusText("Debe Agregar al menos un telefono")
           return
            
        self.PushStatusText("")
        p=ctrlPersona()
            
        tipo_id=self._tipo_id.GetValue();
        identificacion=self._identificacion.GetValue();
        nombres=self._nombres.GetValue();
        fecha_nac=self._fecha.GetValue();
        p.insertar_persona(tipo_id, identificacion, nombres, fecha_nac, self.telefonos)
            
            


    def agregar_telefono(self, evt):
        frame = AgregarTelefonos()
        frame.Show()
        hijos=frame.GetChildren()
        
        #import pdb; pdb.set_trace()
        
    def ValidaId(self, ident):
        patron="^\d{5,8}$";
        if re.match(patron, ident):
            return True
        return False
    
    def ValidaNombre(self):
        pass
    
    def ValidaFecha(self):
        pass

    def showFrame(self, msg):
        print "mensaje recibido %s" % msg
        self.telefonos=msg.data
        if isinstance(list, self.telefonos) and len(self.telefonos)>0:
            self.PushStatusText("Telefonos %s almacenados en memoria " %(",".join(self.telefonos)))
        else:
            self.PushStatusText("No se selecciono ningun telefono" )


if __name__ == "__main__":
    app = RegistroPersonas(False)
    app.MainLoop()
