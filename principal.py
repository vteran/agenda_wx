#!/usr/bin/env python
#-*-coding:utf-8-*-
import wx
from vistas.registro_personas import RegistroPersonas
from vistas.listado_personas import ListadoPersonas

######################################################
"""
Agenda WX
---------
Material de apoyo para cursos python dictado por Victor Francisco Terán Herrera.
Contacto: vteran93 at yahoo dot es
Bogota, Colombia

Descripción de la aplicación:
-----------------------------
Es una aplicación que mediante dos tablas de MySQL usando la libreria elixir
almacena los datos asociados a una persona o empresa. Permite almacenar datos 
básicos.

Copyright (C) <2014>  <Víctor Francisco Terán Herrera>


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
######################################################




Licencia="""
    Agenda wx Copyright (C) <2014>  <Víctor F. Terán Herrera>
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.
"""

if __name__=='__main__':
    app = wx.App(False)
    print Licencia
    frame = ListadoPersonas()
    frame.Show()
    app.MainLoop()
