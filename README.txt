Agenda WX
---------
Material de apoyo para cursos python dictado por Victor Francisco Terán Herrera.

Contacto: vteran93 at yahoo dot es

Bogota, Colombia

Descripción de la aplicación:

Es una aplicación que mediante dos tablas de MySQL usando la libreria elixir
almacena los datos asociados a una persona o empresa. Permite almacenar datos 
básicos.

Dependencias:

mysql-server
mysql-client    
python-dev
python-Mysql
python-mysqldb
python-wxgtk2.8
libmysqlclient
libmysqlclient-dev
python-elixir



